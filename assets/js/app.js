/** @description Item Controller */
const storageController = (() => {
	let storeItem = item => {
		let items;

		if (localStorage.getItem('items') === null) {
			items = [];
			items.push(item);
			localStorage.setItem('items', JSON.stringify(items));
		} else {
			items = JSON.parse(localStorage.getItem('items'));
			items.push(item);
			localStorage.setItem('items', JSON.stringify(items));
		}
	};
	let getItems = function() {
		let items;
		if (localStorage.getItem('items') === null) {
			items = [];
		} else {
			items = JSON.parse(localStorage.getItem('items'));
		}
		return items;
	};
	let updateItemInLocalStorage = updatedItem => {
		let items = JSON.parse(localStorage.getItem('items'));
		items.forEach((item, index) => {
			if (updatedItem.id === item.id) {
				items.splice(index, 1, updatedItem);
			}
		});
		localStorage.setItem('items', JSON.stringify(items));
	};

	let deleteItemFromLocalStorage = id => {
		let items = JSON.parse(localStorage.getItem('items'));
		ids = items.map(item => {
			return item.id;
		});
		const index = ids.indexOf(id);
		items.splice(index, 1);
		localStorage.setItem('items', JSON.stringify(items));
	};

	let deleteAllItemsFromLocalStorage = () => {
		// let items = JSON.parse(localStorage.getItem('items'));
		// items = [];
		// localStorage.setItem('items', JSON.stringify(items));
		localStorage.removeItem('items');
	};

	return {
		storeItem,
		getItems,
		updateItemInLocalStorage,
		deleteItemFromLocalStorage,
		deleteAllItemsFromLocalStorage
	};
})();

/** @description Item Controller */

const itemController = (() => {
	//item class
	class Item {
		constructor(id, name, calories) {
			this.id = id;
			this.name = name;
			this.calories = calories;
		}
	}

	const data = {
		items: storageController.getItems(),
		currentItem: null,
		totalCalories: 0
	};

	/**@description class methods */

	//adds an item to the items array in the data object
	let addItem = (name, calories) => {
		let ID;
		data.items.length > 0
			? (ID = data.items[data.items.length - 1].id + 1)
			: (ID = 0);

		calories = parseInt(calories);
		newItem = new Item(ID, name, calories);
		data.items.push(newItem);

		return newItem;
	};

	let getTotalCalories = () => {
		let total = 0;
		//loop through items and add cals
		data.items.forEach(item => {
			total += item.calories;
		});
		//set data calories to total
		data.totalCalories = total;

		//return total
		return data.totalCalories;
	};

	//logs the items array
	let getItems = () => data.items;

	let getItemById = id => {
		let found = null;

		data.items.forEach(item => {
			if (item.id == id) {
				found = item;
			}
		});
		return found;
	};

	let updateItem = (name, calories) => {
		calories = parseInt(calories);

		let found = null;
		data.items.forEach(item => {
			if (item.id === data.currentItem.id) {
				item.name = name;
				item.calories = calories;
				found = item;
			}
		});
		return found;
	};

	let setCurrentItem = item => {
		data.currentItem = item;
	};

	let getCurrentItem = () => {
		return {
			id: data.currentItem.id,
			name: data.currentItem.name,
			calories: data.currentItem.calories
		};
	};

	let deleteItem = id => {
		ids = data.items.map(item => {
			return item.id;
		});

		const index = ids.indexOf(id);
		data.items.splice(index, 1);
	};

	let deleteAllItems = () => {
		data.items = [];
	};

	//logs the data object
	let logItems = () => data;

	return {
		getItems,
		logItems,
		addItem,
		getTotalCalories,
		getItemById,
		setCurrentItem,
		getCurrentItem,
		updateItem,
		deleteItem,
		deleteAllItems
	};
})();

/** @description UI Controller */

const uiController = (() => {
	const uiSelectors = {
		itemList: '#item-list',
		addButton: '.add-btn',
		updateButton: '.update-btn',
		deleteButton: '.delete-btn',
		listItems: '#item-list li',
		backButton: '.back-btn',
		itemNameInput: '#item-name',
		itemCaloriesInput: '#item-calories',
		totalCalories: '.total-calories',
		clearButton: '.clear-btn'
	};
	// returns the selectors
	let getSelectors = () => uiSelectors;

	let getItemInput = () => {
		var name = document.querySelector(uiSelectors.itemNameInput).value;
		var calories = document.querySelector(uiSelectors.itemCaloriesInput)
			.value;

		return { name, calories };
	};

	//populates list with initial data
	let populateItemList = items => {
		let html = '';
		items.forEach(item => {
			html += `<li id="item-${item.id}"class="collection-item">
                <strong>${item.name}: </strong>
                <em>${item.calories} calories</em>
                <a href="#" class="secondary-content">
                    <i class="edit-item fa fa-pencil"></i>
                </a>
            </li>`;
		});
		document.querySelector(uiSelectors.itemList).innerHTML = html;
	};

	//adds an item to the list
	let addListItem = item => {
		document.querySelector(uiSelectors.itemList).style.display =
			'block';

		//create li element
		const li = document.createElement('li');
		li.className = 'collection-item';
		//Add ID
		li.id = `item-${item.id}`;
		li.innerHTML = `
		 <strong>${item.name}: </strong>
		 <em>${item.calories} Calories</em>
		 <a href="#" class="secondary-content">
		 	<i class="edit-item fa fa-pencil"></i>
		 </a>
		`;
		document
			.querySelector(uiSelectors.itemList)
			.insertAdjacentElement('beforeend', li);
	};

	//clears the input fields after adding
	let clearInput = () => {
		document.querySelector(uiSelectors.itemNameInput).value = '';
		document.querySelector(uiSelectors.itemCaloriesInput).value = '';
	};

	//hides the list if no data
	let hideList = () => {
		document.querySelector(uiSelectors.itemList).style.display = 'none';
	};

	//total calories
	let showTotalCalories = totalCalories => {
		document.querySelector(
			uiSelectors.totalCalories
		).textContent = totalCalories;
	};

	let clearEditState = () => {
		uiController.clearInput();
		document.querySelector(uiSelectors.updateButton).style.display =
			'none';
		document.querySelector(uiSelectors.deleteButton).style.display =
			'none';
		document.querySelector(uiSelectors.backButton).style.display =
			'none';
		document.querySelector(uiSelectors.addButton).style.display =
			'inline';
	};

	let showEditState = () => {
		document.querySelector(uiSelectors.updateButton).style.display =
			'inline';
		document.querySelector(uiSelectors.deleteButton).style.display =
			'inline';
		document.querySelector(uiSelectors.backButton).style.display =
			'inline';
		document.querySelector(uiSelectors.addButton).style.display =
			'none';
	};

	let addItemToForm = () => {
		document.querySelector(
			uiSelectors.itemNameInput
		).value = itemController.getCurrentItem().name;
		document.querySelector(
			uiSelectors.itemCaloriesInput
		).value = itemController.getCurrentItem().calories;

		uiController.showEditState();
	};

	let updateListItem = item => {
		let listItems = document.querySelectorAll(uiSelectors.listItems);
		//turn node list into array
		listItems = Array.from(listItems);
		listItems.forEach(listItem => {
			const itemId = listItem.getAttribute('id');
			if (itemId === `item-${item.id}`) {
				document.querySelector(`#${itemId}`).innerHTML = `
					<strong>${item.name}: </strong>
					<em>${item.calories} Calories</em>
					<a href="#" class="secondary-content">
						<i class="edit-item fa fa-pencil"></i>
					</a>
				`;
			}
		});
	};
	let deleteListItem = id => {
		const itemId = `#item-${id}`;
		const item = document.querySelector(itemId);
		item.remove();
	};
	let deleteAllListItems = () => {
		let listItems = document.querySelectorAll(uiSelectors.listItems);

		listItems = Array.from(listItems);
		listItems.forEach(item => {
			item.remove();
		});
	};

	/**@default revealing methods */
	return {
		uiSelectors,
		populateItemList,
		getSelectors,
		getItemInput,
		addListItem,
		clearInput,
		hideList,
		showTotalCalories,
		clearEditState,
		addItemToForm,
		showEditState,
		updateListItem,
		deleteListItem,
		deleteAllListItems
	};
})();

/** @description App Controller */

const appController = ((itemController, uiController, storageController) => {
	//load event listeners
	const loadEventListeners = function() {
		const { getSelectors } = uiController;
		const uiSelectors = getSelectors();
		//add item event
		document
			.querySelector(uiSelectors.addButton)
			.addEventListener('click', e => {
				//Get form input from UI controller
				const { getItemInput } = uiController;
				const input = getItemInput();

				if (input.name !== '' && input.calories !== '') {
					const { addItem } = itemController;
					const newItem = addItem(
						input.name,
						input.calories
					);
					uiController.addListItem(newItem);

					const totalCalories = itemController.getTotalCalories();

					uiController.showTotalCalories(totalCalories);

					storageController.storeItem(newItem);

					uiController.clearInput();
				}

				e.preventDefault();
			});
		document.addEventListener('keypress', e => {
			if (e.keyCode === 13 || e.which === 13) {
				e.preventDefault();
				return false;
			}
		});
		document
			.querySelector(uiSelectors.itemList)
			.addEventListener('click', e => {
				if (e.target.classList.contains('edit-item')) {
					//Get list item Id

					const listId = e.target.parentNode.parentNode.id;
					const listIdArr = listId.split('-');
					const id = parseInt(listIdArr[1]);

					//get item
					const itemToEdit = itemController.getItemById(id);

					//set current item;
					itemController.setCurrentItem(itemToEdit);

					uiController.addItemToForm();
				}
				e.preventDefault();
			});
		document
			.querySelector(uiSelectors.updateButton)
			.addEventListener('click', e => {
				const input = uiController.getItemInput();
				const updatedItem = itemController.updateItem(
					input.name,
					input.calories
				);
				uiController.updateListItem(updatedItem);

				const totalCalories = itemController.getTotalCalories();
				uiController.showTotalCalories(totalCalories);

				storageController.updateItemInLocalStorage(updatedItem);

				uiController.clearEditState();
				e.preventDefault();
			});
		document
			.querySelector(uiSelectors.deleteButton)
			.addEventListener('click', e => {
				const currentItem = itemController.getCurrentItem();
				uiController.deleteListItem(currentItem.id);
				itemController.deleteItem(currentItem.id);

				const totalCalories = itemController.getTotalCalories();
				uiController.showTotalCalories(totalCalories);

				storageController.deleteItemFromLocalStorage(
					currentItem.id
				);
				uiController.clearEditState();

				e.preventDefault();
			});
		document
			.querySelector(uiSelectors.backButton)
			.addEventListener('click', e => {
				uiController.clearEditState();
				e.preventDefault();
			});
		document
			.querySelector(uiSelectors.clearButton)
			.addEventListener('click', e => {
				itemController.deleteAllItems();
				uiController.deleteAllListItems();

				const totalCalories = itemController.getTotalCalories();
				uiController.showTotalCalories(totalCalories);

				storageController.deleteAllItemsFromLocalStorage();

				uiController.hideList();

				e.preventDefault();
			});
	};

	//initializes the application app root
	let initFunction = () => {
		const { populateItemList, clearEditState } = uiController;
		clearEditState();

		const { getItems } = itemController;
		const items = getItems();
		items.length === 0
			? uiController.hideList()
			: populateItemList(items);

		loadEventListeners();
	};

	/**@default revealing methods */

	return { initFunction };
})(itemController, uiController, storageController);

const { initFunction } = appController;
initFunction();
